# Virtual Wallet

Clone repository
navigate to `virtual-wallet/frontend/admin`

## Install

### yarn

`yarn`

## tests

### linter js

`yarn lint:js`

### linter css

`yarn lint:css`

### test

`yarn test:unit`

### build

`yarn build`

## run

`yarn serve`

Acess `localhost:8080` for fun
