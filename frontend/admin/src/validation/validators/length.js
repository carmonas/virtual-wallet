export function length(lte, gte) {
  return ({value}) => {
    if (!value) return true
    const lgth = getLength(value)
    return lgth >= lte && lgth <= gte
  }
}

function getLength(value) {
  if (Array.isArray(value)) return value.length
  if (typeof value === 'object') {
    return Object.keys(value).length
  }
  return String(value).length
}
