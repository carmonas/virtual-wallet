import {regex} from './regex'

describe('regex', () => {
  it('should validate based on a regex', () => {
    const isNumber = regex(/^[0-9]+$/)
    expect(isNumber({value: 10})).toBe(true)
    expect(isNumber({value: 'dd'})).toBe(false)
  })
})
