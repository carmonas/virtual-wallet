export function required({value}) {
  if (Array.isArray(value)) return !!value.length
  if (value === undefined || value === null || value === '') {
    return false
  }

  if (value === false) {
    return true
  }

  if (value instanceof Date) {
    // invalid date won't pass
    return !isNaN(value.getTime())
  }

  if (typeof value === 'object') {
    for (let _ in value) return true
    return false
  }

  return !!String(value).length
}
