import {isEqual} from 'lodash'

export function changed({value, lastValue, lastStatus}) {
  if (!isEqual(value, lastValue) && !lastStatus) {
    return true
  }
  return lastStatus
}
