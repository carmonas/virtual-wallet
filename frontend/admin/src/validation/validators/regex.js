export function regex(expr) {
  return ({value}) => {
    if (!value) return true
    return expr.test(value)
  }
}
