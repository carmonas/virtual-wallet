import {length} from './length'

describe('length', () => {
  it('should validate length', () => {
    const validation = length(2, 5)
    expect(validation({value: 12})).toBe(true)
    expect(validation({value: [1, 2, 3]})).toBe(true)
    expect(validation({value: 'abc'})).toBe(true)

    expect(validation({value: 2})).toBe(false)
    expect(validation({value: [1]})).toBe(false)
    expect(validation({value: 'a'})).toBe(false)
  })
})
