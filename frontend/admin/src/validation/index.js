import {changed} from '@/validation/validators/changed'
import {email} from '@/validation/validators/email'
import {length} from '@/validation/validators/length'
import {regex} from '@/validation/validators/regex'
import {required} from '@/validation/validators/required'

import {VField, VList, VObject} from '@/validation/types'

export default {
  changed,
  length,
  regex,
  required,
  email,
  VField,
  VList,
  VObject,
  Schema: VObject,
}
