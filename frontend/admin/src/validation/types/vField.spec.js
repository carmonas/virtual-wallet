import {VField} from './vField'
import {FieldState} from './consts'

describe('VField', () => {
  const required = {required: ({value}) => !!value}
  const biggerThanTen = {biggerThanTen: ({value}) => value > 10}

  describe('setValidation', () => {
    it('should add a validation to schema', () => {
      const field = VField({
        $v: [],
      })
      field.setValidation(required)

      expect(field.validations.size).toBe(1)
      expect(field.validations.has('required')).toBe(true)
    })
  })

  describe('validate', () => {
    it('should validate simple fields', () => {
      const field = VField({
        $v: [required, biggerThanTen],
      })

      expect(field.validate({value: undefined})).toEqual(
        FieldState({
          modified: true,
          error: true,
          errors: ['required', 'biggerThanTen'],
        })
      )
      expect(field.validate({value: 5})).toEqual(
        FieldState({modified: true, error: true, errors: ['biggerThanTen']})
      )
      expect(field.validate({value: 11})).toEqual(
        FieldState({modified: true, error: false, errors: []})
      )
    })

    it('should validate based on related field', () => {
      const field = VField({
        $v: [
          {
            biggerThanAttrTwo: ({value, container}) => {
              return value > container.attrTwo
            },
          },
        ],
      })
      const obj = {value: 5, attrTwo: 10}
      const obj2 = {value: 15, attrTwo: 10}

      expect(field.validate({value: obj.value, container: obj})).toEqual(
        FieldState({modified: true, error: true, errors: ['biggerThanAttrTwo']})
      )
      expect(field.validate({value: obj2.value, container: obj2})).toEqual(
        FieldState({modified: true, error: false, errors: []})
      )
    })

    it('should validate based previous validation', () => {
      const field = VField({
        $v: [
          {
            test: ({value, lastValue, lastStatus}) => {
              if (value !== lastValue && !lastStatus) {
                return true
              }
              return lastStatus
            },
          },
        ],
      })

      field.setInitialValue(10)
      const obj = {value: 15}
      const resultOne = field.validate({value: obj.value, container: obj})
      expect(resultOne).toEqual(
        FieldState({modified: true, error: false, errors: []})
      )

      field.reset()
      field.setInitialValue(10)
      field.setError('test')
      field.setModified()
      const obj2 = {value: 10}
      const resultTwo = field.validate({value: obj2.value, container: obj2})
      expect(resultTwo).toEqual(
        FieldState({error: true, modified: true, errors: ['test']})
      )
    })
  })
})
