import {FieldState, validationTypes} from './consts'
import {VField} from './vField'

export const VListField = listSchema => {
  let modified = false

  return {
    type: validationTypes.LIST,

    setModified() {
      modified = true
    },

    unsetModified() {
      modified = false
    },

    validate({array}) {
      let subfields = {}
      let error = false
      let fieldSchema = VField(listSchema)

      for (let idx = 0; idx < array.length; ++idx) {
        subfields[idx] = fieldSchema.validate({
          value: array[idx],
          container: array,
        })

        error = error || subfields[idx].$error
        modified = modified || subfields[idx].$modified
      }

      return FieldState({error, errors: [], subfields})
    },
  }
}
