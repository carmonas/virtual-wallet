import {VField} from './vField'
import {FieldState} from './consts'
import {VObjectList} from './vObjectList'

describe('VObjectList', () => {
  const required = {required: ({value}) => !!value}

  it('should validate an array of objects', () => {
    const arrayField = VObjectList({
      name: VField({
        $v: [
          required,
          {valid: ({value}) => value.length > 3 && value.length < 30},
        ],
      }),
      email: VField({
        $v: [{regex: ({value}) => /\@[a-zA-Z]+\.(com|net)/.test(value)}],
      }),
    })

    const validInput = [{name: 'testing', email: 'something@mail.com'}]
    const invalidInput = [{name: '', email: 'noemail'}]

    expect(arrayField.validate({array: validInput})).toEqual(
      FieldState({
        error: false,
        modified: true,
        errors: [],
        subfields: {
          '0': FieldState({
            error: false,
            modified: true,
            errors: [],
            subfields: {
              name: FieldState({error: false, modified: true, errors: []}),
              email: FieldState({error: false, modified: true, errors: []}),
            },
          }),
        },
      })
    )
    expect(arrayField.validate({array: invalidInput})).toEqual(
      FieldState({
        error: true,
        errors: [],
        modified: true,
        subfields: {
          '0': FieldState({
            error: true,
            errors: [],
            modified: true,
            subfields: {
              name: FieldState({
                modified: true,
                error: true,
                errors: ['required', 'valid'],
              }),
              email: FieldState({
                error: true,
                modified: true,
                errors: ['regex'],
              }),
            },
          }),
        },
      })
    )
  })
})
