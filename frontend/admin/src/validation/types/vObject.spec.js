import {FieldState} from './consts'
import {VField} from './vField'
import {VObject} from './vObject'

describe('VObject', () => {
  const required = {required: ({value}) => !!value}
  const biggerThanTen = {biggerThanTen: ({value}) => value > 10}

  it('should add validation to a field', () => {
    const objectField = VObject({
      name: VField({$v: []}),
    })
    const fail = {name: ''}
    const success = {name: 'required false'}

    objectField.setValidation('name', required)
    objectField.setModifiedField('name')

    expect(objectField.validate({value: fail})).toEqual(
      FieldState({
        error: true,
        errors: [],
        modified: true,
        subfields: {
          name: FieldState({
            modified: true,
            error: true,
            errors: ['required'],
          }),
        },
      })
    )
    expect(objectField.validate({value: success})).toEqual(
      FieldState({
        error: false,
        errors: [],
        modified: true,
        subfields: {
          name: FieldState({modified: true, error: false, errors: []}),
        },
      })
    )
  })

  it('should validate a object', () => {
    const objectField = VObject({
      $v: [{rule: obj => typeof obj === 'object'}],
      name: VField({
        $v: [
          required,
          {valid: ({value}) => value.length > 3 && value.length < 30},
        ],
      }),
      email: VField({
        $v: [{regex: ({value}) => /\@[a-zA-Z]+\.(com|net)/.test(value)}],
      }),
    })

    const validInput = {name: 'testing', email: 'something@mail.com'}
    const invalidInput = {name: '', email: 'noemail'}

    expect(objectField.validate({value: validInput})).toEqual(
      FieldState({
        error: false,
        errors: [],
        modified: true,
        subfields: {
          name: FieldState({error: false, modified: true, errors: []}),
          email: FieldState({error: false, modified: true, errors: []}),
        },
      })
    )
    expect(objectField.validate({value: invalidInput})).toEqual(
      FieldState({
        error: true,
        errors: [],
        modified: true,
        subfields: {
          name: FieldState({
            error: true,
            modified: true,
            errors: ['required', 'valid'],
          }),
          email: FieldState({error: true, modified: true, errors: ['regex']}),
        },
      })
    )
  })
})
