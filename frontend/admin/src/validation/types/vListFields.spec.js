import {FieldState} from './consts'
import {VListField} from './vListFields'

describe('VListField', () => {
  const required = {required: ({value}) => !!value}
  const biggerThanTen = {biggerThanTen: ({value}) => value > 10}

  it('should validate a list of values', () => {
    const listField = VListField({
      $v: [required, biggerThanTen],
    })

    expect(listField.validate({array: [undefined, 5, 11]})).toEqual(
      FieldState({
        error: true,
        errors: [],
        subfields: {
          '0': FieldState({
            modified: true,
            error: true,
            errors: ['required', 'biggerThanTen'],
          }),
          '1': FieldState({
            modified: true,
            error: true,
            errors: ['biggerThanTen'],
          }),
          '2': FieldState({modified: true, error: false, errors: []}),
        },
      })
    )
  })
})
