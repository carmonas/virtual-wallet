import {FieldState, validationTypes, validationsToMap} from './consts'
import {VField} from '@/validation/types/vField'
import {isUndefined} from 'lodash'

// Responsável por validar uma lista de VObjects
export const VList = listSchema => {
  let modified = false
  let lastValue = []
  let lastCallStatus = {}

  return {
    type: validationTypes.LIST,
    schema: listSchema,
    validations: validationsToMap(listSchema.$v),
    validationItem: validationsToMap(listSchema.$item),

    setModified() {
      modified = true
    },

    unsetModified() {
      modified = false
    },

    setError(name) {
      lastCallStatus[name] = false
    },

    setValid(name) {
      lastCallStatus[name] = true
    },

    setInitialValue(array) {
      lastValue = array
    },

    reset() {
      lastCallStatus = {}
      lastValue = []
      modified = false
    },

    validate({value, container, oldStatus}) {
      let items = {}
      let error = false
      let failed = []
      let itemSchema = listSchema.$itemSchema || VField({})

      if (isUndefined(container)) {
        container = value
      }

      if (!isUndefined(this.schema.$v)) {
        for (let [name, isValid] of this.validations.entries()) {
          const context = {value, lastValue, container, oldStatus}
          const status = isValid(context)
          if (!status) {
            failed.push(name)
          }
          lastCallStatus[name] = status
        }
      }

      for (let idx = 0; idx < value.length; ++idx) {
        items[idx] = itemSchema.validate({
          value: value[idx],
          container,
        })
        error = Boolean(error || items[idx].$error)
        modified = Boolean(modified || items[idx].$modified)
      }

      error = error || failed.length > 0

      return FieldState({error, modified, errors: failed, subfields: items})
    },
  }
}
