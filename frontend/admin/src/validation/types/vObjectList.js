import {FieldState, validationTypes} from './consts'
import {VObject} from './vObject'

// Responsável por validar uma lista de VObjects
export const VObjectList = listSchema => {
  let modified = false

  return {
    type: validationTypes.OBJECT_LIST,

    setModified() {
      modified = true
    },

    unsetModified() {
      modified = false
    },

    validate({array}) {
      let items = {}
      let error = false
      let objSchema = VObject(listSchema)

      for (let idx = 0; idx < array.length; ++idx) {
        items[idx] = objSchema.validate({value: array[idx], container: array})
        error = error || items[idx].$error
        modified = modified || items[idx].$modified
      }

      return FieldState({error, modified, errors: [], subfields: items})
    },
  }
}
