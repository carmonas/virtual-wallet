import {FieldState, validationTypes, validationsToMap} from './consts'
import {isUndefined} from 'lodash'

// Responsável pela validação de um 'plain field'
export const VField = fieldSchema => {
  let lastValue = ''
  let modified = false
  let lastCallStatus = {}

  return {
    type: validationTypes.PLAIN,
    schema: fieldSchema,
    validations: validationsToMap(fieldSchema.$v || []),

    setValidation(validation) {
      const name = Object.keys(validation)[0]
      this.validations.set(name, validation[name])
    },

    setError(name) {
      lastCallStatus[name] = false
    },

    setValid(name) {
      lastCallStatus[name] = true
    },

    setModified() {
      modified = true
    },

    unsetModified() {
      modified = false
    },

    setInitialValue(value) {
      lastValue = value
    },

    reset() {
      lastCallStatus = {}
      lastValue = ''
      modified = false
    },

    validate(context) {
      const failed = []
      modified = modified || context.value !== lastValue

      for (let [name, validation] of this.validations.entries()) {
        let lastStatus = isUndefined(lastCallStatus[name])
          ? true
          : lastCallStatus[name]

        let fieldContext = {...context, lastStatus, lastValue}
        let status = validation(fieldContext)
        if (!status) {
          failed.push(name)
        }
        lastCallStatus[name] = status
      }
      lastValue = context.value
      return FieldState({error: failed.length >= 1, modified, errors: failed})
    },
  }
}
