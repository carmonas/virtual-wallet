import {VField} from '@/validation/types/vField'
import {FieldState} from '@/validation/types/consts'
import {vList, VList} from './vList'
import {VObject} from '@/validation/types/vObject'

describe('vList', () => {
  const required = {required: ({value}) => !!value}

  it('should validate the array', () => {
    const length = len => ({value}) => value.length >= len
    const arrayField = VList({
      $v: [{length: length(2)}],
      $itemSchema: VField({$v: []}),
    })
    const validState = FieldState({
      error: false,
      modified: true,
    })

    const validInput = [1, 2, 3]
    const invalidInput = [1]

    expect(arrayField.validate({value: validInput})).toEqual(
      FieldState({
        error: false,
        modified: true,
        errors: [],
        subfields: {
          0: validState,
          1: validState,
          2: validState,
        },
      })
    )
    expect(arrayField.validate({value: invalidInput})).toEqual(
      FieldState({
        error: true,
        modified: true,
        errors: ['length'],
        subfields: {
          0: validState,
        },
      })
    )
  })

  it('should validate all items of array', () => {
    const arrayField = VList({
      $v: [],
      $itemSchema: VObject({
        name: VField({
          $v: [
            required,
            {valid: ({value}) => value.length > 3 && value.length < 30},
          ],
        }),
        email: VField({
          $v: [{regex: ({value}) => /\@[a-zA-Z]+\.(com|net)/.test(value)}],
        }),
      }),
    })

    const validInput = [{name: 'testing', email: 'something@mail.com'}]
    const invalidInput = [{name: 'a', email: 'noemail'}]

    expect(arrayField.validate({value: validInput})).toEqual(
      FieldState({
        error: false,
        modified: true,
        errors: [],
        subfields: {
          '0': FieldState({
            error: false,
            modified: true,
            errors: [],
            subfields: {
              name: FieldState({error: false, modified: true, errors: []}),
              email: FieldState({error: false, modified: true, errors: []}),
            },
          }),
        },
      })
    )
    expect(arrayField.validate({value: invalidInput})).toEqual(
      FieldState({
        error: true,
        errors: [],
        modified: true,
        subfields: {
          '0': FieldState({
            error: true,
            errors: [],
            modified: true,
            subfields: {
              name: FieldState({
                modified: true,
                error: true,
                errors: ['valid'],
              }),
              email: FieldState({
                error: true,
                modified: true,
                errors: ['regex'],
              }),
            },
          }),
        },
      })
    )
  })
})
