export const validationTypes = {
  PLAIN: 'plain',
  LIST: 'list',
  OBJECT: 'object',
  OBJECT_LIST: 'object_list',
}

export const FieldState = ({
  error = false,
  modified = false,
  errors = [],
  subfields = {},
} = {}) => {
  return {
    $error: error,
    $errors: errors,
    $subfields: subfields,
    $modified: modified,
    $showError: Boolean(modified && errors.length > 0),
  }
}

// Transforma o $v em um Map
export const validationsToMap = $v => {
  const validations = new Map()
  if (!Array.isArray($v)) {
    return validations
  }
  for (let i = 0; i < $v.length; ++i) {
    const validation = $v[i]
    const name = Object.keys(validation)[0]
    validations.set(name, validation[name])
  }
  return validations
}
