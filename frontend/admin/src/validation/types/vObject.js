import {FieldState, validationTypes, validationsToMap} from './consts'
import {cloneDeep, forOwn, isUndefined} from 'lodash'

// Valida um objeto composto por VFields
export const VObject = objSchema => {
  let modified = false
  let lastValue = {}
  let lastCallStatus = {}

  return {
    type: validationTypes.OBJECT,
    schema: objSchema,
    validations: validationsToMap(objSchema.$v),

    setValidation(field, validation) {
      if (!this.schema[field]) {
        return
      }
      this.schema[field].setValidation(validation)
    },

    setError(name) {
      lastCallStatus[name] = false
    },

    setErrorField(field, failedValidations = []) {
      for (let name of failedValidations) {
        this.schema[field].setError(name)
      }
    },

    setValid(field, name) {
      this.schema[field].setValid(name)
    },

    setModified() {
      modified = true
    },

    setModifiedField(field) {
      this.schema[field].setModified()
    },

    unsetModifiedField(field) {
      this.schema[field].unsetModified()
    },

    setModifiedFields() {
      for (let field in this.schema) {
        if (field !== '$v') {
          this.schema[field].setModified()
        }
      }
    },

    unsetModified() {
      modified = false
    },

    setInitialValue(value) {
      lastValue = value
      for (let field in value) {
        if (this.schema[field]) {
          this.schema[field].setInitialValue(value[field])
        }
      }
    },

    reset() {
      modified = false
      for (let field in this.schema) {
        if (field !== '$v') {
          this.schema[field].reset()
        }
      }
    },

    validate({value, container, oldStatus} = {}) {
      let subfields = {}
      let error = false
      let failed = []

      if (!isUndefined(this.schema.$v)) {
        for (let [name, isValid] of this.validations.entries()) {
          let lastStatus = isUndefined(lastCallStatus[name])
            ? true
            : lastCallStatus[name]

          const context = {value, lastValue, lastStatus, container}
          const status = isValid(context)
          if (!status) {
            failed.push(name)
          }
          lastCallStatus[name] = status
        }
      }

      if (isUndefined(container)) {
        container = value
      }

      forOwn(value, (attr, name) => {
        if (name === '$v' || !this.schema[name]) return

        const context = {
          value: value[name],
          container,
        }
        subfields[name] = this.schema[name].validate(context)
        error = Boolean(error || subfields[name].$error)
        modified = Boolean(modified || subfields[name].$modified)
      })

      error = Boolean(error || failed.length > 0)
      lastValue = cloneDeep(value)
      return FieldState({error, modified, errors: failed, subfields})
    },
  }
}
