import AppNotification from './AppNotification.vue'

export default {
  install(vue) {
    vue.component('app-notification', AppNotification)
  },
}
