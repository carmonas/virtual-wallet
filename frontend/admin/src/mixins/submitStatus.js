import {STATUS} from '@/consts'

export function submitStatus({value, $router, notify}) {
  if (value === STATUS.empty || value === STATUS.inProgress) return
  if (value === STATUS.ok) {
    $router.push(this.backLink)
  } else {
    notify('submit.error')
  }
}

export default {
  watch: {
    ['submitStatus'](value) {
      submitStatus({value, $router: this.$router, notify: this.notify})
    },
  },
}
