import {isError, isLoading} from '@/consts'

export const fetchStatus = {
  computed: {
    loading() {
      return isLoading(this.fetchStatus)
    },
    error() {
      return isError(this.fetchStatus)
    },
  },
}
