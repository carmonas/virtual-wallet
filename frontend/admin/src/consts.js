export const STATUS = {
  empty: '',
  inProgress: 'inProgress',
  ok: 'ok',
  notFound: 'notFound',
  notReady: 'notReady',
  invalid: 'invalid',
  duplicated: 'duplicated',
  unauthenticated: 'unauthenticated',
  forbidden: 'forbidden',
  error: 'error',
}
