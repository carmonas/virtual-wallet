import notificationStore, {notificationStatus} from './notification'

describe('store/notifications', () => {
  let commit
  let state
  const {actions, mutations} = notificationStore

  beforeEach(() => {
    commit = jest.fn()
    state = {
      message: '',
      active: false,
      status: notificationStatus.NONE,
      timeout: 3000,
    }
  })

  describe('actions', () => {
    it('should throw an error when has no message', () => {
      const dialogMessage = {
        status: notificationStatus.ERROR,
        timeout: 1000,
      }
      expect(() => actions.sendNotification(ctx, dialogMessage)).toThrowError()
    })

    it('should add notification to dialog', () => {
      const dialogMessage = {
        status: notificationStatus.ERROR,
        timeout: 1000,
        text: 'O item possui um servidor associado',
      }

      actions.sendNotification({commit}, dialogMessage)

      expect(commit).toBeCalledWith('notificate', {
        status: notificationStatus.ERROR,
        timeout: 1000,
        text: 'O item possui um servidor associado',
      })
    })

    it('should deactivate a notification', () => {
      actions.closeNotification({commit})
      expect(commit).toBeCalledWith('resetNotification')
    })
  })

  describe('mutations', () => {
    it('should add a notification to state', () => {
      mutations.notificate(state, {
        status: notificationStatus.ERROR,
        timeout: 1000,
        text: 'testing',
      })
      expect(state).toEqual({
        active: true,
        message: 'testing',
        status: notificationStatus.ERROR,
        timeout: 1000,
      })
    })

    it('should turn of the notification', () => {
      state = {
        message: 'message',
        timeout: 1000,
        status: notificationStatus.ERROR,
      }
      mutations.resetNotification(state)
      expect(state).toEqual({
        active: false,
        status: notificationStatus.NONE,
        timeout: 3000,
        message: '',
      })
    })
  })
})
