/**
 * Status possíveis de notificações
 */
export const notificationStatus = {
  NONE: 'NONE',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
  WARNING: 'WARNING',
}

const DEFAULT_TIMEOUT = 3000

const state = {
  active: false,
  status: notificationStatus.NONE,
  message: '',
  timeout: DEFAULT_TIMEOUT,
}

const actions = {
  sendNotification({commit}, {timeout, text, status}) {
    if (!text) throw new Error('Nenhuma mensagem para a notificação')

    commit('notificate', {text, status, timeout})
    setTimeout(() => {
      commit('resetNotification')
    }, timeout || DEFAULT_TIMEOUT)
  },

  closeNotification({commit}) {
    commit('resetNotification')
  },
}

const mutations = {
  notificate(state, {timeout, text, status}) {
    state.message = text
    state.status = status
    state.timeout = timeout || state.timeout
    state.active = true
  },

  resetNotification(state) {
    state.active = false
    state.message = ''
    state.status = notificationStatus.NONE
    state.timeout = DEFAULT_TIMEOUT
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
