import DefaultService from '@/services/default'
import {localStore} from '@/utils/localStorage'

const state = {
  account: {},
  brita: {
    valueBuy: 0,
    valueSell: 0,
    total: 0,
  },
  bitcoin: {
    valueBuy: 0,
    valueSell: 0,
    total: 0,
  },
  status: '',
}

const actions = DefaultService => ({
  async setAccount({commit}, {real, brita, bitcoin}) {
    localStore.set('account', {
      real: real,
      brita: brita,
      bitcoin: bitcoin,
    })

    let valDollar = await DefaultService.getDollar()
    let valBitcoin = await DefaultService.getBitcoin()

    commit('setAccount', {real, brita, bitcoin})
    commit('setBrita', {
      valueBuy: valDollar.cotacaoCompra,
      valueSell: valDollar.cotacaoVenda,
      total: valDollar.cotacaoVenda * brita,
    })
    commit('setBitcoin', {
      valueBuy: valBitcoin.buy,
      valueSell: valBitcoin.sell,
      total: valBitcoin.sell * bitcoin,
    })
  },
  async getAccount({commit, dispatch}) {
    let account = localStore.get('account')
    if (account) {
      commit('setAccount', account)

      let valDollar = await DefaultService.getDollar()
      let valBitcoin = await DefaultService.getBitcoin()

      commit('setBrita', {
        valueBuy: valDollar.cotacaoCompra,
        valueSell: valDollar.cotacaoVenda,
        total: valDollar.cotacaoVenda * account.brita,
      })
      commit('setBitcoin', {
        valueBuy: valBitcoin.buy,
        valueSell: valBitcoin.sell,
        total: valBitcoin.sell * account.bitcoin,
      })
    } else {
      dispatch('setAccount', {
        real: '100000',
        brita: '0',
        bitcoin: '0',
      })
    }
  },
})

const mutations = {
  setAccount(state, account) {
    state.account = account
  },
  setBrita(state, brita) {
    state.brita = brita
  },
  setBitcoin(state, bitcoin) {
    state.bitcoin = bitcoin
  },
  setStatus(state, status) {
    state.status = status
  },
}

export function createStore(service) {
  return {
    namespaced: true,
    actions: actions(service),
    mutations,
    state,
  }
}

export default createStore(new DefaultService())
