import Vue from 'vue'
import Vuex from 'vuex'
import defaultStore from '@/store/default'
import notification from '@/store/notification'

import extract from '@/pages/extract/store'
import transaction from '@/pages/transaction/store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    defaultStore,
    notification,
    extract,
    transaction,
  },
})
