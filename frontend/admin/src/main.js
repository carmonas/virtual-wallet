import App from './App.vue'
import Vue from 'vue'
import VueCurrencyFilter from 'vue-currency-filter'
import Vuetify from 'vuetify'
import applicationComponents from '@/components'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'

import '@/assets/css/default.scss'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'

Vue.config.productionTip = false

Vue.use(applicationComponents)

Vue.use(Vuetify, {
  theme: {
    primary: colors.purple.darken1,
    secondary: colors.grey.darken1,
    accent: colors.blue.darken3,
    error: colors.red.darken1,
    info: colors.blue.accent3,
    success: colors.green.accent2,
    warning: colors.yellow.darken4,
  },
})

Vue.use(VueCurrencyFilter, {
  symbol: 'R$',
  thousandsSeparator: '.',
  fractionCount: 2,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true,
})

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
