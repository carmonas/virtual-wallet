import DefaultLayout from '@/layouts/Default.vue'
import Router from 'vue-router'
import Vue from 'vue'

import extractList from '@/pages/extract/view/index'
import transaction from '@/pages/transaction/view/index'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'base',
      component: DefaultLayout,
      children: [
        {
          path: '/extract',
          name: 'extract',
          component: extractList,
        },
        {
          path: '/transaction',
          name: 'transaction',
          component: transaction,
        },
      ],
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
})

export default router
