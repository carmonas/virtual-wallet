import axios from 'axios'

export default class TransitionService {
  async getDollar() {
    let url =
      'https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)'
    let data = "'09-04-2018'"
    let resp = await axios
      .get(url, {
        params: {
          '@dataCotacao': data,
          $format: 'json',
        },
      })
      .catch()
    return resp.data.value[0]
  }
  async getBitcoin() {
    let url = 'https://www.mercadobitcoin.net/api/BTC/ticker/'
    let resp = await axios.get(url).catch()
    return resp.data.ticker
  }
}
