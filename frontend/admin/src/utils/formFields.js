import {FieldState} from '@/validation/types'
import {isEqual, isPlainObject, isUndefined} from 'lodash'

/**
 * Representa um campo de form
 * @param {Object} params
 */
export const Field = ({name, value, $fields = [], $list = false} = {}) => {
  const defaultValue = typeof value === 'undefined' ? ($list ? [] : '') : value

  return {
    name,
    $list,
    $fields,
    default: defaultValue,

    isPrimitive() {
      return $fields.length === 0
    },
  }
}

/**
 * @param {Field[]} fields
 */
export function createForm(fields) {
  let form = {}
  for (let field of fields) {
    form[field.name] = field.isPrimitive()
      ? field.default
      : createForm(field.$fields)
  }
  return form
}

/**
 * Cria o state de validação para um campo
 * @param {String} field
 */
function createFieldValidations(field) {
  let validations = FieldState()

  if (!field.isPrimitive()) {
    for (let subfield of field.$fields) {
      const subfieldValidations = createFieldValidations(subfield)
      validations.$subfields[subfield.name] = subfieldValidations
    }
  }

  return validations
}

/**
 * Cria o state de validação para um conjunto de campos
 * @param {Field[]} fields
 */
export function createValidations(fields) {
  let validations = FieldState()

  for (let field of fields) {
    validations[field.name] = createFieldValidations(field)
  }

  return validations
}

/**
 * Atualiza os valores dos campos no state
 * @param {ValidationState} validations
 * @param {Object} form
 * @param {Object} data
 */
export function updateFields(validations, form, data) {
  let anyModified = false

  for (let field in data) {
    let value = data[field]

    if (isPlainObject(value)) {
      anyModified = updateFields(validations[field], form[field], value)
    } else if (!isEqual(value, form[field])) {
      anyModified = true
      form[field] = value

      if (!isUndefined(validations[field])) {
        validations[field].$modified = true
        validations[field].$showError = validations[field].$error
      }
    }
  }

  anyModified = validations.$modified || anyModified
  validations.$modified = anyModified
  validations.$showError = validations.$error && anyModified
  return anyModified
}

/**
 * Atualiza o state baseado nos erros de validação
 * @param {ValidationState} validations
 * @param {FieldState} rootErrors
 */
export function updateErrors(validations, rootErrors) {
  const errors = rootErrors.subfields
  validations.$error = rootErrors.error

  for (let field in validations) {
    if (!errors[field]) continue

    if (errors[field].subfields.length > 0) {
      updateErrors(validations[field], errors[field])
    }

    let fieldErrors = errors[field]
    validations[field].$error = fieldErrors.error
    validations[field].$errorType = fieldErrors.error
      ? fieldErrors.errors[0]
      : ''
  }
}
