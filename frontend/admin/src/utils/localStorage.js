import engine from 'store/src/store-engine'
import localStorage from 'store/storages/localStorage'

export const localStore = engine.createStore(localStorage)
