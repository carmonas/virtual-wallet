import {errorMessage} from './i18n'

describe('errorMessages', () => {
  const i18n = jest.fn()

  afterEach(() => {
    i18n.mockReset()
  })

  it('should return undefined', () => {
    expect(errorMessage({i18n, error: undefined})).toBe('')
    expect(errorMessage({i18n, error: ''})).toBe('')
  })

  it('should get a specific message', () => {
    i18n.mockImplementation(() => {
      return 'testing validation'
    })
    const error = 'regex'
    const messages = {
      regex: {id: 'form.errors.testing.regex'},
    }

    const result = errorMessage({i18n, error, messages})

    expect(result).toEqual('testing validation')
    expect(i18n).toBeCalledWith('form.errors.testing.regex')
  })

  it('should get a generic message', () => {
    i18n.mockImplementation(() => {
      return 'testing generic validation'
    })
    const error = ['regex']
    const messages = {}

    const result = errorMessage({i18n, error, messages})

    expect(result).toEqual('testing generic validation')
    expect(i18n).toBeCalledWith('form.errors.regex')
  })
})
