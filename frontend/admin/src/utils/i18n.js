import {isUndefined} from 'lodash'

const GENERIC_MESSAGES = {
  required: 'form.errors.required',
  length: 'form.errors.length',
  regex: 'form.errors.regex',
  duplicated: 'form.errors.duplicated',
  email: 'form.errors.email',
  passwordScore: 'form.errors.passwordScore',
}

export function errorMessage({i18n, error, messages = {}} = {}) {
  if (isUndefined(error) || error === '') {
    return ''
  } else if (isUndefined(messages[error])) {
    return i18n(GENERIC_MESSAGES[error])
  }
  return i18n(messages[error].id)
}
