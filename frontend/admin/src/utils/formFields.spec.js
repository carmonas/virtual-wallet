import {updateFields, createForm, Field, updateErrors} from './formFields'
import {FieldState} from '@/validation/types'
import validation from '@/validation/index'

describe('formFields', () => {
  let state

  beforeEach(() => {
    state = {
      form: {},
      validations: {},
    }
  })

  describe('createState', () => {
    it('should create initial state with simple fields', () => {
      const fields = [
        Field({name: 'str', value: ''}),
        Field({name: 'bool', value: false}),
        Field({name: 'num', value: 0}),
        Field({name: 'list', value: [], $list: true}),
      ]

      const form = createForm(fields)

      expect(form).toEqual({
        str: '',
        bool: false,
        num: 0,
        list: [],
      })
    })

    it('should create state recursively', () => {
      const fields = [
        Field({
          name: 'testing',
          $fields: [Field({name: 'tst', value: 0})],
        }),
      ]
      const expectedValidationState = FieldState({
        subfields: {
          tst: FieldState(),
        },
      })

      const form = createForm(fields)

      expect(form).toEqual({
        testing: {
          tst: 0,
        },
      })
    })
  })

  describe('updateFields', () => {
    it('should update all changed fields', () => {
      const state = {
        form: {
          propChanged: 'changed',
          propNotChanged: 'not changed',
        },
        validations: {
          propChanged: FieldState(),
          propNotChanged: FieldState(),
        },
      }
      const data = {
        propChanged: 'changed to this',
        propNotChanged: 'not changed',
      }

      updateFields(state.validations, state.form, data)

      expect(state.validations.propChanged.$modified).toBe(true)
      expect(state.form.propChanged).toBe(data.propChanged)
    })

    it('should update fields recursively', () => {
      const state = {
        form: {
          propChanged: {
            insideProp: '',
            test: {
              t: 0,
              j: 5,
            },
          },
        },
        validations: {
          propChanged: {
            insideProp: FieldState(),
            test: {
              t: FieldState(),
            },
          },
        },
      }
      const data = {
        propChanged: {
          insideProp: 'with value',
          test: {
            t: 10,
          },
        },
      }

      updateFields(state.validations, state.form, data)

      expect(state.form.propChanged.insideProp).toBe('with value')
      expect(state.form.propChanged.test.t).toBe(10)
      expect(state.form.propChanged.test.j).toBe(5)
    })
  })
})
