import axios from 'axios'
import {STATUS} from '@/consts'
import {localStore} from '@/utils/localStorage'

export default class TransactionService {
  constructor() {}

  async saveTransaction(transaction) {
    await localStore.set('account', {
      real: transaction.account.real,
      brita: transaction.account.brita,
      bitcoin: transaction.account.bitcoin,
    })
    this.updateExtractList(transaction)
    return {status: STATUS.ok}
  }

  async updateExtractList(transaction) {
    let list = (await localStore.get('ExtractList'))
      ? localStore.get('ExtractList').list
      : []
    transaction = {
      type: transaction.type,
      date: new Date(),
      real: transaction.account.real,
      coin: transaction.coin,
      brita:
        transaction.coin === 'brita' || transaction.type === 'trade'
          ? transaction.account.brita
          : '-',
      bitcoin:
        transaction.coin === 'bitcoin' || transaction.type === 'trade'
          ? transaction.account.bitcoin
          : '-',
    }
    list.push(transaction)
    localStore.set('ExtractList', {
      list,
    })
    return {status: STATUS.ok}
  }

  async getDollar() {
    let url =
      'https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)'
    let data = "'09-04-2018'"
    let resp = await axios
      .get(url, {
        params: {
          '@dataCotacao': data,
          $format: 'json',
        },
      })
      .catch()
    return {
      status: STATUS.ok,
      brita: resp.data.value[0],
    }
  }
  async getBitcoin() {
    let url = 'https://www.mercadobitcoin.net/api/BTC/ticker/'
    let resp = await axios.get(url).catch()
    return {
      status: STATUS.ok,
      bitcoin: resp.data.ticker,
    }
  }
  async getAccount() {
    let account = await localStore.get('account')
    return {
      status: STATUS.ok,
      account,
    }
  }
}
