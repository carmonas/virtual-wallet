import TransactionService from './services'
import {ACTIONS, MUTATIONS} from './consts'
import {
  Field,
  createForm,
  createValidations,
  updateFields,
} from '@/utils/formFields'
import {STATUS} from '@/consts'
import {cloneDeep} from 'lodash'
import {transactionSchema} from '@/pages/transaction/schemas'

const fields = [
  Field({name: 'type', value: 'buy'}),
  Field({name: 'selectedCoins', value: 'bitcoin'}),
  Field({name: 'amountCoins', value: '0'}),
]

const initialState = {
  transaction: createForm(fields),
  validations: createValidations(fields),
  isTransactionValid: false,
  coins: [
    {
      text: 'Bitcoin',
      value: 'bitcoin',
    },
    {
      text: 'Brita',
      value: 'brita',
    },
  ],
  account: {},
  brita: {},
  bitcoin: {},
}

const state = cloneDeep(initialState)

const mutations = {
  [MUTATIONS.clean](state) {
    const cleanState = cloneDeep(initialState)
    state.transaction = cleanState.transaction
    transactionSchema.reset()
  },
  [MUTATIONS.cleanValidation](state) {
    const cleanState = cloneDeep(initialState)
    state.validations = cleanState.validations
    transactionSchema.reset()
  },
  [MUTATIONS.setFormErrors](state, errors) {
    state.validations = errors.$subfields
    state.isTransactionValid = !errors.$error
  },
  [MUTATIONS.updateForm](state, transtion) {
    state.isTransactionValid = true
    updateFields(state.validations, state.transaction, transtion)
  },
  [MUTATIONS.setAccount](state, account) {
    state.account = account
  },
  [MUTATIONS.setBrita](state, brita) {
    state.brita = brita
  },
  [MUTATIONS.setBitcoin](state, bitcoin) {
    state.bitcoin = bitcoin
  },
}

const actions = TransactionService => ({
  [ACTIONS.clean]({state, commit}) {
    commit(MUTATIONS.clean)
  },
  [ACTIONS.updateForm]({commit, state}, field) {
    commit(MUTATIONS.cleanValidation)
    commit(MUTATIONS.updateForm, field)
  },
  async [ACTIONS.getBrita]({commit, dispatch}, account) {
    let resp = await TransactionService.getDollar()

    if (resp.status === STATUS.ok) {
      commit(MUTATIONS.setBrita, {
        buy: resp.brita.cotacaoCompra,
        sell: resp.brita.cotacaoVenda,
        total: resp.brita.cotacaoVenda * account.brita,
      })
    }
  },
  async [ACTIONS.getBitcoin]({commit, dispatch}, account) {
    let resp = await TransactionService.getBitcoin()

    if (resp.status === STATUS.ok) {
      commit(MUTATIONS.setBitcoin, {
        buy: resp.bitcoin.buy,
        sell: resp.bitcoin.sell,
        total: resp.bitcoin.sell * account.bitcoin,
      })
    }
  },
  async [ACTIONS.getAccount]({commit, dispatch}) {
    let resp = await TransactionService.getAccount()
    if (resp.status === STATUS.ok) commit(MUTATIONS.setAccount, resp.account)
  },
  async [ACTIONS.saveTransaction]({commit, dispatch, state}) {
    await dispatch(ACTIONS.getAccount)
    await dispatch(ACTIONS.getBritas)
    await dispatch(ACTIONS.getBitcoin)
    let container = {
      transaction: state.transaction,
      brita: state.brita,
      bitcoin: state.bitcoin,
      account: state.account,
    }

    const errors = transactionSchema.validate({
      value: state.transaction,
      container: container,
    })

    if (errors.$error) {
      commit(MUTATIONS.setFormErrors, errors)
    } else {
      let transaction = {
        type: state.transaction.type,
        amountCoins: state.transaction.amountCoins,
        coin: state.transaction.selectedCoins,
      }
      let coin = state.transaction.selectedCoins
      let resp

      if (state.transaction.type === 'buy') {
        let valueCoinBuy = state.transaction.amountCoins * state[coin].buy

        transaction = {
          ...transaction,
          account: {
            real: parseFloat(state.account.real) - parseFloat(valueCoinBuy),
            brita:
              coin === 'brita'
                ? parseInt(state.transaction.amountCoins) +
                  parseInt(state.account[coin])
                : parseInt(state.account.brita),
            bitcoin:
              coin === 'bitcoin'
                ? parseInt(state.transaction.amountCoins) +
                  parseInt(state.account[coin])
                : parseInt(state.account.bitcoin),
          },
        }
        resp = await TransactionService.saveTransaction(transaction)
      } else if (state.transaction.type === 'sell') {
        let valueCoinSell =
          parseInt(state.transaction.amountCoins) * state[coin].sell
        transaction = {
          ...transaction,
          account: {
            real: parseFloat(state.account.real) + parseFloat(valueCoinSell),
            brita:
              coin === 'brita'
                ? parseInt(state.account[coin]) -
                  parseInt(state.transaction.amountCoins)
                : parseInt(state.account.brita),
            bitcoin:
              coin === 'bitcoin'
                ? parseInt(state.account[coin]) -
                  parseInt(state.transaction.amountCoins)
                : parseInt(state.account.bitcoin),
          },
        }
        resp = await TransactionService.saveTransaction(transaction)
      } else if (state.transaction.type === 'trade') {
        let valueCoinTrade =
          parseInt(state.transaction.amountCoins) * parseFloat(state[coin].sell)
        transaction = {
          ...transaction,
          account: {
            real:
              parseFloat(state.account.real) +
              (valueCoinTrade % parseFloat(state[coin].buy)),
            brita:
              coin === 'brita'
                ? parseInt(state.account[coin]) -
                  parseInt(state.transaction.amountCoins)
                : parseInt(state.account.brita) +
                  parseInt(valueCoinTrade / state.brita.buy),
            bitcoin:
              coin === 'bitcoin'
                ? parseInt(state.account[coin]) -
                  parseInt(state.transaction.amountCoins)
                : parseInt(state.account.bitcoin) +
                  parseInt(valueCoinTrade / state.bitcoin.buy),
          },
        }
        resp = await TransactionService.saveTransaction(transaction)
      }
      if (resp.status === STATUS.ok) {
        dispatch('defaultStore/getAccount', null, {root: true})
      }
    }
  },
})

export function createStore(service) {
  return {
    namespaced: true,
    actions: actions(service),
    mutations,
    state,
  }
}

export default createStore(new TransactionService())
