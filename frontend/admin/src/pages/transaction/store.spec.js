import {createStore} from './store'
import {ACTIONS, MUTATIONS} from './consts'
import {STATUS} from '@/consts'
import {FieldState} from '@/validation/types'
import {transactionSchema} from '@/pages/transaction/schemas'

describe('transaction store', () => {
  const services = {
    saveTransaction: jest.fn(),
    updateExtractList: jest.fn(),
    getDollar: jest.fn(),
    getBitcoin: jest.fn(),
    getAccount: jest.fn(),
  }

  const transactionStore = createStore(services)
  const {actions, mutations} = transactionStore
  const commit = jest.fn()
  const dispatch = jest.fn()

  const fakeTransaction = {
    type: 'buy',
    selectedCoins: 'bitcoin',
    amountCoins: '2',
  }

  const fakeAccount = {
    real: '45000',
    bitcoin: '2',
    brita: '250',
  }

  const fakeBitcoin = {
    buy: '27500',
    sell: '27650',
  }

  const fakeBrita = {
    cotacaoCompra: '4.20',
    cotacaoVenda: '4.10',
  }

  afterEach(() => {
    commit.mockReset()
    dispatch.mockReset()
  })

  describe('actions', () => {
    describe(ACTIONS.clean, () => {
      it('should clear status', () => {
        actions[ACTIONS.clean]({commit})
        expect(commit).toBeCalledWith(MUTATIONS.clean)
      })
    })

    describe(ACTIONS.getAccount, () => {
      afterEach(() => services.getAccount.mockReset())

      it('should set account to state', async () => {
        services.getAccount.mockImplementation(() => {
          return {
            status: STATUS.ok,
            account: fakeAccount,
          }
        })

        await actions[ACTIONS.getAccount]({commit, dispatch})

        expect(commit).toBeCalledWith(MUTATIONS.setAccount, fakeAccount)
      })
    })
  })

  describe(ACTIONS.getBrita, () => {
    afterEach(() => services.getDollar.mockReset())

    it('should set brita to state', async () => {
      services.getDollar.mockImplementation(() => {
        return {
          status: STATUS.ok,
          brita: fakeBrita,
        }
      })

      await actions[ACTIONS.getBrita]({commit, dispatch}, fakeAccount)

      expect(commit).toBeCalledWith(MUTATIONS.setBrita, {
        buy: '4.20',
        sell: '4.10',
        total: 1025,
      })
    })
  })

  describe(ACTIONS.getBitcoin, () => {
    afterEach(() => services.getBitcoin.mockReset())

    it('should set bitcoin to state', async () => {
      services.getBitcoin.mockImplementation(() => {
        return {
          status: STATUS.ok,
          bitcoin: fakeBitcoin,
        }
      })

      await actions[ACTIONS.getBitcoin]({commit, dispatch}, fakeAccount)

      expect(commit).toBeCalledWith(MUTATIONS.setBitcoin, {
        buy: '27500',
        sell: '27650',
        total: 55300,
      })
    })
    describe(ACTIONS.saveTransaction, () => {
      afterEach(() => services.saveTransaction.mockReset())
      const state = {
        transaction: {
          type: '',
          selectedCoins: '',
          amountCoins: '',
        },
        account: fakeAccount,
        brita: {
          buy: '4.20',
          sell: '4.10',
          total: 1025,
        },
        bitcoin: {
          buy: '27500',
          sell: '27650',
          total: 55300,
        },
      }

      it('should be a buy transaction', async () => {
        state.transaction = {
          type: 'buy',
          selectedCoins: 'brita',
          amountCoins: '2',
        }
        services.saveTransaction.mockImplementation(() => {
          return {status: STATUS.ok}
        })

        await actions[ACTIONS.saveTransaction]({commit, state, dispatch})
        expect(services.saveTransaction).toBeCalledWith({
          type: 'buy',
          amountCoins: '2',
          coin: 'brita',
          account: {
            real: 44991.6,
            brita: 252,
            bitcoin: 2,
          },
        })
      })
      it('should be a sell transaction', async () => {
        state.transaction = {
          type: 'sell',
          selectedCoins: 'bitcoin',
          amountCoins: '1',
        }
        services.saveTransaction.mockImplementation(() => {
          return {status: STATUS.ok}
        })

        await actions[ACTIONS.saveTransaction]({commit, state, dispatch})
        expect(services.saveTransaction).toBeCalledWith({
          type: 'sell',
          amountCoins: '1',
          coin: 'bitcoin',
          account: {
            real: 72650,
            brita: 250,
            bitcoin: 1,
          },
        })
      })
      it('should be a trade transaction', async () => {
        state.transaction = {
          type: 'trade',
          selectedCoins: 'bitcoin',
          amountCoins: '1',
        }
        services.saveTransaction.mockImplementation(() => {
          return {status: STATUS.ok}
        })

        await actions[ACTIONS.saveTransaction]({commit, state, dispatch})
        expect(services.saveTransaction).toBeCalledWith({
          type: 'trade',
          amountCoins: '1',
          coin: 'bitcoin',
          account: {
            real: 45150,
            brita: 6833,
            bitcoin: 1,
          },
        })
      })
    })
  })

  describe('mutations', () => {
    describe(MUTATIONS.setFormErrors, () => {
      it('should set form errors', () => {
        const state = {validations: {}}
        const errors = FieldState({
          error: true,
          modified: true,
          subfields: {
            ammoutCoins: FieldState({
              modified: true,
              error: true,
              errors: ['required'],
            }),
          },
        })
        mutations[MUTATIONS.setFormErrors](state, errors)
        expect(state.validations).toEqual(errors.$subfields)
        expect(state.isTransactionValid).toEqual(false)
      })
    })

    describe(MUTATIONS.setAccount, () => {
      it('should set account', () => {
        const state = {account: {}}
        const account = {
          real: '45000',
          bitcoin: '2',
          brita: '250',
        }
        mutations[MUTATIONS.setAccount](state, account)
        expect(state.account).toEqual(account)
      })
    })

    describe(MUTATIONS.setBitcoin, () => {
      it('should set bitcoin', () => {
        const state = {bitcoin: {}}
        const bitcoin = {
          buy: '27500',
          sell: '27650',
        }
        mutations[MUTATIONS.setBitcoin](state, bitcoin)
        expect(state.bitcoin).toEqual(bitcoin)
      })
    })

    describe(MUTATIONS.setBrita, () => {
      it('should set britas', () => {
        const state = {brita: {}}
        const brita = {
          cotacaoCompra: '4.20',
          cotacaoVenda: '4.10',
        }
        mutations[MUTATIONS.setBrita](state, brita)
        expect(state.brita).toEqual(brita)
      })
    })
  })
})
