import vld from '@/validation'
import {VField, VObject} from '@/validation/types/index'

export const transactionSchema = VObject({
  amountCoins: VField({
    $v: [
      {required: vld.required},
      {
        buy: ({value, container}) => {
          if (container.transaction.type !== 'buy') return true
          let coin = container.transaction.selectedCoins
          let valueBuy = parseInt(value) * container[coin].buy
          return !(valueBuy > container.account.real)
        },
      },
      {
        sell: ({value, container}) => {
          if (container.transaction.type !== 'sell') return true
          let coin = container.transaction.selectedCoins
          return !(
            container.account[coin] <= 0 || container.account[coin] < value
          )
        },
      },
      {
        trade: ({value, container}) => {
          if (container.transaction.type !== 'trade') return true
          let coin = container.transaction.selectedCoins
          let coinNumber = container.account[coin]
          if (coinNumber < value) return false
          let valueCoins = parseFloat(value) * container[coin].sell
          if (coin === 'bitcoin') return !(valueCoins < container.brita.buy)
          else return !(valueCoins < container.bitcoin.buy)
        },
      },
    ],
  }),
})
