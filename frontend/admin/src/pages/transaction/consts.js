export const ACTIONS = {
  saveTransaction: 'save_transaction',
  TRADE: 'trade',
  BUY: 'buy',
  SELL: 'sell',
  clean: 'clean',
  fetchUser: 'fetch_transaction',
  saveUser: 'save_transaction',
  updateForm: 'update_form',
  getAccount: 'get_account',
  getBrita: 'get_brita',
  getBitcoin: 'get_bitcoin',
}

export const MUTATIONS = {
  clean: 'clean',
  setActionStatus: 'set_action_status',
  setFormErrors: 'set_form_errors',
  updateForm: 'update_form',
  cleanValidation: 'clean_validation',
  setAccount: 'set_account',
  setBrita: 'set_brita',
  setBitcoin: 'set_bitcoin',
}
