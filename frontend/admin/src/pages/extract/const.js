export const ACTIONS = {
  getExtractList: 'get_extract_list',
}

export const MUTATIONS = {
  setList: 'set_list',
}
