import {STATUS} from '@/consts'
import {localStore} from '@/utils/localStorage'

export default class ExtractList {
  constructor() {}

  async getList() {
    let extracts = (await localStore.get('ExtractList'))
      ? localStore.get('ExtractList').list
      : []

    return {extracts, status: STATUS.ok}
  }
}
