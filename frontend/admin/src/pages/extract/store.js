import ExtractService from './services'
import {ACTIONS, MUTATIONS} from './const'
import {cloneDeep} from 'lodash'

const initialState = {
  list: [],
}

const state = cloneDeep(initialState)

const mutations = {
  [MUTATIONS.setList](state, list) {
    state.list = list
  },
}

const actions = ExtractService => ({
  async [ACTIONS.getExtractList]({commit, dispatch}) {
    let resp = await ExtractService.getList()
    commit(MUTATIONS.setList, resp.extracts)
  },
})

export function createStore(service) {
  return {
    namespaced: true,
    actions: actions(service),
    mutations,
    state,
  }
}

export default createStore(new ExtractService())
