import Vue from 'vue'
import VueI18n from 'vue-i18n'
import pt_BR from '@/i18n/pt_br'

Vue.use(VueI18n)

export default new VueI18n({
  locale: 'pt_BR',
  messages: {
    pt_BR,
  },
})
