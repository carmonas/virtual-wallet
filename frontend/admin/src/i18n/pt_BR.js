export default {
  notfound: 'Não encontrado',
  unauthorized: 'Não autorizado',
  submitError: 'Erro ao salvar',
  required: 'Este campo é requerido',
  length: 'Tamanho do campo inválido',
  breadcrumbs: {
    edit: 'Editar',
  },
  form: {
    errors: {
      required: 'Campo é requerido',
      duplicated: 'Campo duplicado',
    },
  },
}
