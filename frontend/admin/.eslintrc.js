module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'eslint:recommended',
    'standard',
    'plugin:vue/recommended',
    '@vue/prettier',
  ],
  plugins: ['jest', 'prettier', 'vue', 'sort-imports-es6-autofix'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'max-len': [
      2,
      {
        ignoreComments: true,
        ignoreUrls: true,
        ignoreRegExpLiterals: true,
        ignorePattern:
          '^(\\s*(var|const|let)\\s.+=\\s*require\\s*\\(.*\\)|.*\\s*from\\s+(\\\'|\\").*(\\\'|\\"))$',
      },
    ],
    semi: ['error', 'never'],
    'comma-dangle': ['error', 'always-multiline'],
    'no-unused-expressions': 2,
    'key-spacing': [
      'error',
      {
        mode: 'strict',
        beforeColon: false,
        afterColon: true,
      },
    ],
    'object-curly-spacing': ['error', 'never'],
    'prettier/prettier': [
      'error',
      {
        semi: false,
        singleQuote: true,
        trailingComma: 'es5',
        bracketSpacing: false,
      },
    ],
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/valid-expect': 'error',
    'sort-imports-es6-autofix/sort-imports-es6': [
      'error',
      {memberSyntaxSortOrder: ['none', 'all', 'single', 'multiple']},
    ],
    'vue/max-attributes-per-line': [
      2,
      {
        singleline: 3,
        multiline: {
          max: 1,
          allowFirstLine: false,
        },
      },
    ],
  },
}
