module.exports = {
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest',
  },
  transformIgnorePatterns: ['node_modules'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  testMatch: [
    '**/src/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)',
  ],
  testPathIgnorePatterns: ['<rootDir>/node_modules'],
  testURL: 'http://localhost/',
  setupFiles: ['<rootDir>/test-vue.js'],
  watchPathIgnorePatterns: ['/node_modules/', '/jspm_packages/', '/coverage/'],
  snapshotSerializers: ['<rootDir>/node_modules/jest-serializer-vue'],
}
